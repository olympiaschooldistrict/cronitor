package cronitor

import (
	"fmt"
	"testing"

	_ "github.com/joho/godotenv/autoload"
)

func failer() error {
	return fmt.Errorf("Made to fail")
}
func succeeder() error {
	return nil
}
func panicker() error {
	panic("I'm panicking")
}

func TestRunFromEnv(t *testing.T) {
	type args struct {
		f func() error
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"panicFunction", args{panicker}, true},
		{"failFunction", args{failer}, true},
		{"succeedFunction", args{succeeder}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := RunFromEnv(tt.args.f); (err != nil) != tt.wantErr {
				t.Errorf("RunFromEnv() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
