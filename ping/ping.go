package ping

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"strings"
)

// https://cronitor.io/docs/ping-api
// Endpoint	Description	Used For
// /run	Report job is running	Jobs
// /complete	Report job is complete	Jobs
// /tick	Report an event occurred private beta	Events
// /fail	Report a failure/error	Jobs   Events
// /ok	Manually reset a monitor to a healthy state	Jobs   Events
// /pause/<hours>	Pause alerting for given <hours>	Jobs

// Resource	Description could be a monitorId or a pingAPIkey and nice name
// Monitor	cronitor.link/:monitorId/:event
// Universal	cronitor.link/ping/:pingAPIKey/:uniqueId/:event
type ResourceDescription string
type CronitorErr error

// Returns a ResourceDescription with the MonitorId pulled from the cronitor.io dashboard
func NewMonitorResource(monitorId string) *ResourceDescription {
	if monitorId == "" {
		return nil
	}
	monitorId = strings.TrimSuffix(monitorId, "/") + "/"
	rd := ResourceDescription(monitorId)
	return &rd
}

// Will create new Monitor with uniqiueId name if passed a working pingAPIKey. if pingAPIKey is "" will try env var "CRONITOR_PING_API_KEY"
func NewUniversalId(pingAPIKey, uniqueId string) *ResourceDescription {
	if pingAPIKey == "" {
		pingAPIKey = os.Getenv("CRONITOR_PING_API_KEY")
	}
	if pingAPIKey == "" || uniqueId == "" {
		return nil
	}
	rd := ResourceDescription(fmt.Sprintf("ping/%s/%s/", url.PathEscape(pingAPIKey), url.PathEscape(uniqueId)))
	return &rd
}

// Will create new Monitor with uniqiueId name using env var "CRONITOR_PING_API_KEY"
func NewUniversalIdWithEnv(uniqueId string) *ResourceDescription {
	pingAPIKey := os.Getenv("CRONITOR_PING_API_KEY")
	if pingAPIKey == "" || uniqueId == "" {
		return nil
	}
	rd := ResourceDescription(fmt.Sprintf("ping/%s/%s/", url.PathEscape(pingAPIKey), url.PathEscape(uniqueId)))
	return &rd
}

// Exec pings Run immediately and then if f returns a nil error, will ping Complete with the msg string as a message, if f returns an error or panics, it will ping Fail with the error or panic message
// the returned indicates if the message was apparently recieved by cronitor.io
func (r *ResourceDescription) Exec(f func() (msg string, err error)) (err error) {
	err = r.Run(nil)
	if err != nil {
		return err
	}
	defer func() {
		if re := recover(); re != nil {
			rerr := fmt.Errorf("Recovered Panic: %v", re)
			err = r.Fail(&Params{Message: rerr.Error()})
		}
	}()
	msg, ferr := f()
	if ferr != nil {
		return r.Fail(&Params{Message: ferr.Error()})
	}
	return r.Complete(&Params{Message: msg})
}

func (r *ResourceDescription) ExecPassThroughError(f func() (msg string, err error)) (err error) {
	r.Run(nil)
	defer func() {
		if re := recover(); re != nil {
			rerr := fmt.Errorf("Recovered Panic: %v", re)
			r.Fail(&Params{Message: rerr.Error()})
			err = rerr
		}
	}()
	msg, ferr := f()
	if ferr != nil {
		r.Fail(&Params{Message: ferr.Error()})
		return ferr
	}
	r.Complete(&Params{Message: msg})
	return nil
}

// Params is Parameters for the pings to be sent
type Params struct {
	Message    string //    message	run, complete, fail	A url-encoded message, up to 2000 chars
	Host       string //    host		run, complete, fail	The name of the server sending this request
	Series     string //    series		run, complete, fail	A unique user-supplied ID to collate related pings
	StatusCode string //    status_code	 complete, fail	Exit code returned from the task
	// AuthKey    string // auth_key	run, complete, fail, pause	Ping API key. Required if Ping API authentication is enabled.
	// Duration   string // duration		 complete, fail	Total runtime as float
}

// Run sends a single ping to the run endpoint of the ResourceDescription
// the returned indicates if the message was apparently recieved by cronitor.io
func (r *ResourceDescription) Run(q *Params) (err error) {
	u, err := getURL(r, run, q)
	if err != nil {
		return
	}
	return do(u)
}

// Complete sends a single ping to the complete endpoint of the ResourceDescription
// the returned indicates if the message was apparently recieved by cronitor.io
func (r *ResourceDescription) Complete(q *Params) (err error) {
	u, err := getURL(r, complete, q)
	if err != nil {
		return
	}
	return do(u)
}

// Fail sends a single ping to the fail endpoint of the ResourceDescription
// the returned indicates if the message was apparently recieved by cronitor.io
func (r *ResourceDescription) Fail(q *Params) (err error) {
	u, err := getURL(r, fail, q)
	if err != nil {
		return
	}
	return do(u)
}

type endpoint struct {
	Event       string
	Description string
}

type endpointCode int

const (
	run endpointCode = iota
	complete
	// Tick
	fail
	// Ok
	// Pause
)

var endpointURL = map[endpointCode]string{
	run:      "run",
	complete: "complete",
	fail:     "fail",
}

var paramFilter = map[endpointCode][]string{
	run:      []string{`message`, `host`, `series`, `auth_key`},
	complete: []string{`message`, `host`, `series`, `auth_key`, `status_code`, `duration`},
	fail:     []string{`message`, `host`, `series`, `auth_key`, `status_code`, `duration`},
	// Ok:       []string{},
	// Pause:    []string{`auth_key`},
	// Tick:  []string{`message`, `host`, `series`, `auth_key`, `status_code`, `duration`},
}

func (q *Params) query() url.Values {
	v := url.Values{}
	if q == nil {
		return v
	}
	if q.Message != "" {
		v.Add("message", url.QueryEscape(q.Message))
	}
	if q.Host != "" {
		v.Add("host", url.QueryEscape(q.Host))
	} else {
		envhost := os.Getenv("CRONITOR_HOSTNAME")
		if envhost != "" {
			v.Add("host", url.QueryEscape(envhost))
		} else {
			n, _ := os.Hostname()
			if n != "" {
				v.Add("host", url.QueryEscape(n))
			}
		}
	}
	if q.Series != "" {
		v.Add("series", url.QueryEscape(q.Series))
	}
	if q.StatusCode != "" {
		v.Add("status_code", url.QueryEscape(q.StatusCode))
	}

	return v
}

func do(url string) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	if resp.StatusCode == http.StatusOK {
		return nil
	}
	dec := json.NewDecoder(resp.Body)
	result := struct {
		Title string `json:"title"`
	}{}
	dec.Decode(&result)
	resp.Body.Close()
	return fmt.Errorf("status %v %v", resp.Status, result.Title)
}

func getURL(r *ResourceDescription, e endpointCode, q *Params) (string, error) {
	if r == nil {
		return "", fmt.Errorf("resourceDescription is nil")
	}
	if *r == "" {
		return "", fmt.Errorf("resourceDescription is empty")
	}
	u, err := url.Parse("https://cronitor.link/")
	if err != nil {
		return "", err
	}
	u.Path = string(*r)
	u = u.ResolveReference(&url.URL{Path: endpointURL[e]})
	preQ := q.query()
	postQ := url.Values{}
	for _, f := range paramFilter[e] {
		_, ok := preQ[f]
		if ok {
			postQ.Set(f, preQ.Get(f))
		}
	}
	u.RawQuery = postQ.Encode()
	return u.String(), nil
}
