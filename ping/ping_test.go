package ping

import (
	"fmt"
	"os"
	"testing"
)

func TestResourceDescription_Exec(t *testing.T) {
	pingAPIKey := os.Getenv("CRONITOR_PING_API_KEY")
	goodRD := NewUniversalId(pingAPIKey, "testy")
	// badRD := NewMonitorResource("xxxxxxxx")
	type args struct {
		f func() (string, error)
	}
	tests := []struct {
		name    string
		r       *ResourceDescription
		args    args
		wantErr bool
	}{
		{"panicFunction", goodRD, args{panicker}, false},
		{"failFunction", goodRD, args{failer}, false},
		{"succeedFunction", goodRD, args{succeeder}, false},
		// {"panicFunction", goodMRD, args{panicker}, false},
		// {"failFunction", goodMRD, args{failer}, false},
		// {"succeedFunction", goodMRD, args{succeeder}, false},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.r.Exec(tt.args.f); (err != nil) != tt.wantErr {
				t.Errorf("ResourceDescription.Exec() error = %v, wantErr %v", err, tt.wantErr)
			}
			// time.Sleep(100 * time.Millisecond)
		})
	}
}

func failer() (string, error) {
	return "", fmt.Errorf("Made to fail")
}
func succeeder() (string, error) {
	return "all good", nil
}
func panicker() (string, error) {
	panic("I'm panicking")
	// return "shouldn't see me", fmt.Errorf("Made to panic, you shouldn't get here")
}
