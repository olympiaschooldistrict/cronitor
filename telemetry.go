package cronitor

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"
)

type TelemetryURL url.URL

const base = `https://cronitor.link/p/`

var client = &http.Client{
	Timeout: time.Second * 10,
}

// NewFromEnv uses env vars "CRONITOR_PING_API_KEY" and "CRONITOR_API_KEY" to make a simple cronitor telemetry connector
func NewFromEnv() (*TelemetryURL, error) {
	apikey, err := requireEnv("CRONITOR_PING_API_KEY")
	if err != nil {
		return nil, err
	}
	monitorKey, err := requireEnv("CRONITOR_API_KEY")
	if err != nil {
		return nil, err
	}
	return New(apikey, monitorKey)
}

// New makes a simple cronitor telemetry connector
func New(apiKey string, monitorKey string) (*TelemetryURL, error) {
	u, err := url.Parse(base)
	if err != nil {
		return nil, err
	}
	result := u.JoinPath(apiKey, monitorKey)
	return (*TelemetryURL)(result), nil
}

var ErrNil = fmt.Errorf("unexpected nil value")

// Run sends a run event to cronitor, then executes f and sends a fail event (with message) if it panics or return a non-nil err and then log.Fatal, otherwise it sends a complete event, an error returned indicates it failed to send a get request to the cronitor service
func (u *TelemetryURL) Run(f func() error) (err error) {
	if u == nil {
		return ErrNil
	}
	defer func() {
		if r := recover(); r != nil {
			msg := fmt.Errorf("panic! %v", r)
			u.fail(msg.Error())
			err = msg
			// log.Fatalln(msg)
		}
	}()

	err = u.run()
	if err != nil {
		return err
	}
	ferr := f()
	if ferr != nil {
		u.fail(ferr.Error())
		return ferr
		// log.Fatalln(ferr.Error())
	}
	return u.complete()
}

// RunFromEnv creates using NewFromEnv and executes Run as described above
func RunFromEnv(f func() error) error {
	u, err := NewFromEnv()
	if err != nil {
		return err
	}
	return u.Run(f)
}

func Must(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func requireEnv(key string) (val string, err error) {
	val, ok := os.LookupEnv(key)
	if !ok {
		err = fmt.Errorf("env var %s is not set", key)
		return
	}
	if val == "" {
		err = fmt.Errorf("env var %s is blank", key)
		return
	}
	return
}

func (u *TelemetryURL) run() error {
	q := url.Values{"state": []string{"run"}}
	nu, _ := (*url.URL)(u).Parse("")
	nu.RawQuery = q.Encode()
	_, err := client.Get(nu.String())
	return err
}
func (u *TelemetryURL) complete() error {
	q := url.Values{"state": []string{"complete"}}
	nu, _ := (*url.URL)(u).Parse("")
	nu.RawQuery = q.Encode()
	_, err := client.Get(nu.String())
	return err
}
func (u *TelemetryURL) fail(msg string) error {
	q := url.Values{"state": []string{"fail"}}
	nu, _ := (*url.URL)(u).Parse("")
	q.Set("msg", msg)
	nu.RawQuery = q.Encode()
	_, err := client.Get(nu.String())
	return err
}
